# fine-assistant

> Assistant to catch up doings, made with [Electron](https://github.com/atom/electron)

*See [awesome-electron](https://github.com/sindresorhus/awesome-electron) for more useful Electron resources.*

Based on [Electron Boilerplate](https://github.com/sindresorhus/electron-boilerplate/) by [Sindre Sorhus](https://sindresorhus.com)

## License

MIT © [Vsevolod Chernikov](https://chernikov.in) & Alexander Filimonov
